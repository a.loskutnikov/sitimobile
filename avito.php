<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
require_once('mysql_connect.php');
require_once('lib/mysql_qw.php');
require_once('modules/catalog/catalog_lib.php');
header('Content-Type: text/xml; charset=utf-8');

$str = '<?xml version="1.0" encoding="utf-8"?>
<Ads formatVersion="3" target="Avito.ru"></Ads>';

$offers = new SimpleXMLElement($str);

$result = mysql_qw('
  SELECT
    m.*,
    t.id AS cat_id, t.name AS cat_name, t.parent_id AS cat_parent,
    ac.name AS avito_goods_type, ac.parent_id AS avito_cat_id,
    pac.name AS avito_cat
  FROM fa_mobile AS m
  INNER JOIN fa_type AS t ON m.parent_id = t.id AND t.avito_cat_id > 0
  INNER JOIN fa_avito_cats AS ac ON ac.id = t.avito_cat_id
  LEFT JOIN fa_avito_cats AS pac ON pac.id = ac.parent_id
  WHERE is_have=1
  ORDER BY m.id DESC
') or die(mysql_error());
for ($mobiles = array(); $row = mysql_fetch_assoc($result); $mobiles[] = $row);

foreach($mobiles as $mobile)
{
    $offer = $offers->addChild('Ad');

    $offer->addChild('Id', $mobile['id']);
	
	if (!empty($mobile['avito_id'])) {
		$offer->addChild('AvitoId', $mobile['avito_id']);
	}
	
    $offer->addChild('Region', 'Москва');
    $offer->addChild('Subway', 'Ленинский проспект');
	

    if ($mobile['avito_cat_id'] == 0) {
        $mobile['avito_cat'] = $mobile['avito_goods_type'];
        $mobile['avito_goods_type'] = NULL;
    }

    $offer->addChild('Category', xml_safe($mobile['avito_cat']));

    if (!empty($mobile['avito_goods_type'])) {
        $offer->addChild('GoodsType', xml_safe($mobile['avito_goods_type']));
    }

    $offer->addChild('Title', xml_safe($mobile['name']));

	if (!empty($mobile['avito_descr'])) {
		$descr = $mobile['avito_descr'];
		$descr = strip_tags($descr);
		$descr = xml_safe($descr);
	} else {
		$descr = $mobile['descr'];
		$descr = preg_replace('/(<br\s*?\/?>|<\/tr>)/i', "\n", $descr);
		$descr = strip_tags($descr);
		$descr = str_replace('&nbsp;', ' ', $descr);
		$descr = xml_safe($descr);
		$descr = mb_substr($descr, 0, 2900, 'utf-8');
	}
    
    $offer->addChild('Description', $descr);

    $price = ceil($mobile['currency'] == 840 ? GetPriceRUR($mobile['cost']) : $mobile['cost']);
    $offer->addChild('Price', $price);

    $result = mysql_qw('
      SELECT photo FROM fa_photo
      WHERE mobile_id=?
      ORDER BY pos ASC
    ', $mobile['id']) or die(mysql_error());
    for ($photos = array(); $row = mysql_fetch_assoc($result); $photos[] = $row['photo']);

    if (!empty($photos)) {
        $images = $offer->addChild('Images');

        foreach ($photos as $photo) {
            if (empty($photo)) {
                continue;
            }

            $image = $images->addChild('Image');
            $image->addAttribute('url', 'http://www.sitimobile.ru/' . $photo);
        }
    }
}

echo $offers->saveXML();

function xml_safe($value)
{
    $value = htmlspecialchars($value);
    return mb_convert_encoding($value, 'utf-8', 'cp1251');
}
exit;