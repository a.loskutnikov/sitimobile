<?php
## ----------------------------------------
##
## fishAdmin 2.0
##
## ������������ ���������������� ������
##
## ----------------------------------------

error_reporting(E_ALL);

require_once "ftk_config.php";

require_once "mysql_connect.php";

require_once "lib/dumper.php";
require_once "lib/mysql_qw.php";
require_once "lib/redirect.php";
require_once "lib/bmp.php";
require_once "lib/imagecreatefromfile.php";

require_once "admin/ftk_system_lib.php";
require_once "admin/ftk_mod_users.php";
require_once "admin/ftk_mod_catalog.php";
require_once "admin/ftk_mod_catalog_ops.php";
require_once "admin/ftk_mod_catalog_zakaz.php";
require_once "admin/ftk_mod_podbor.php";
require_once "admin/ftk_mod_news.php";
require_once "admin/ftk_mod_static.php";
require_once "admin/ftk_mod_spec.php";
require_once "admin/ftk_mod_photo.php";
require_once "admin/ftk_mod_help.php";
require_once "admin/ftk_mod_feedback.php";
require_once "admin/ftk_mod_options.php";
require_once "admin/ftk_mod_nkvd.php";    

include("fckeditor/fckeditor.php");

## ������ ���������
## ----------------

## SYSTEM: ������������ ����� �������� ������������
if (@$_REQUEST['doAddUser'])
{
 if($_POST['id']==0)
  DoAddUser();
 else
  DoEditUser();
}
## SYSTEM: ������������ ������ ������
elseif (@$_REQUEST['doChangePass'])
{
 DoChangePass();
}
## SYSTEM: ������������ ������� ������� ������
elseif (@$_REQUEST['doDeleteUser'])
{
 DoDeleteUser();
}
## SYSTEM: ������������ ���������
elseif (@$_REQUEST['doLogin'])
{
 DoLogin();
}
/*## CATALOG: ������������ �������� � �������
elseif (@$_REQUEST['doVendor'])
{
 DoVendor();
}
## CATALOG: ������������ �������� � �������� ���������� � ����������� ���������
elseif (@$_REQUEST['doProps'])
{
 DoProps();
}
##  CATALOG: ������������ �������� � ����������
elseif (@$_REQUEST['doMobileEditForm'])
{
 DoMobile();
}               */
## NEWS: �������
elseif(@$_REQUEST['doNews'])
{
 DoNews();
}
## PHOTO: ����
elseif(@$_REQUEST['doPhoto'])
{
  DoPhoto();
}
## STATIC: ����������� ��������
elseif (@$_REQUEST['doStatic'])
{
 DoStatic();
}
## �������
elseif (@$_REQUEST['doCatalog'])
{
 DoCatalog();
}
## �������: ������
elseif (@$_REQUEST['doPodbor'])
{
 DoPodbor();
}
## �������: ���������
elseif (@$_REQUEST['doCatalogOps'])
{
 DoCatalogOps();
}
## �������: ������
elseif (@$_REQUEST['doZakaz'])
{
 DoZakaz();
}
## �������: �������� ��������� ����
elseif (@$_REQUEST['doEditMobileMass'])
{
 DoEditMobileMass();
}
/*## DENTAMED DENTAMED DENTAMED: �������
elseif (@$_REQUEST['doPhone'])
{
 DoPhone();
}    */
##  ���������������
elseif (@$_REQUEST['doSpec'])
{
 DoSpec();
}
## �������� �����
elseif (@$_REQUEST['doFeedback'])
{
 DoFeedback();
}
## ���������
elseif (@$_REQUEST['doOptions'])
{
 DoOptions();
}
## ������
elseif (@$_REQUEST['doSendHelp'])
{
 DoHelp();
}
## ������������ ������ �������� ������
else
{
    // ���� ��� ��� �� ������ ������������, ���� �������
    if(CheckExistsUsers()===false)
    {
      UsersForm(1);
      exit();
    }

    // ���� ���� ������
    if(isset($_COOKIE['ftk_gb_user']) && isset($_COOKIE['ftk_gb_pass']) && CheckRights())
    {
        switch (@$_GET['mode'])
        {
            case "news":
             NewsForm();
             break;
            case "static":
             StaticForm();
             break;
            case "feedback":
             FeedbackForm();
             break;
            case "help":
             HelpForm();
             break;
            case "catalog_ops":
             CatalogOpsForm();
             break;
            case "catalog":
             CatalogForm();
             break;
            case "zakaz":
             ZakazForm();
             break;
            case "podbor":
             PodborForm();
             break;
            case "users":
             UsersForm();
             break;
            case "options":
             OptionsForm();
             break;
            case "spec":
             SpecForm();
             break;
            case "add_user":
             NewUserForm();
             break;
            case "logout":
             Logout();
             break;
            case "change_pass":
             ChangePassForm();
             break;
            case "delete_user":
             DeleteUserForm();
             break;
            case "story":
             NkvdForm();
             break;
            default:
             ShowMenu();
             break;
        }
    }
    else
    {
        LoginForm();
        exit();
    }
}
?>