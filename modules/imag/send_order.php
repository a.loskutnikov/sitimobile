<?php
require_once ("mysql_connect.php");
require_once "lib/dumper.php";
require_once "lib/mysql_qw.php";
require_once "lib/activate_links.php";
require_once "lib/redirect.php";
require_once "admin/ftk_system_lib.php";

@session_start();

if(@$_REQUEST['DoSendOrder'] && isset($_POST['iagree']) && CheckData())
{
    $order = $_SESSION['basket_out'];
	$delivery = $_SESSION['delivery'];
	if ($delivery['price'] == 0)
		$delivery['price'] = '���������';
	$order = str_replace('{DELIVERY}', '�������� ('.$delivery['label'].'): '.$delivery['price'].' ���.', $order);
	$order = str_replace('{COST_ALL}', $_SESSION['cost_all'], $order);

    $adm_mail[0] = GetOption("cat_mail");
    $adm_mail[1] = GetOption("cat_mail2");
    $adm_mail[2] = GetOption("cat_mail3");
    $adm_mail[3] = GetOption("cat_mail4");

    $dt = $_POST;
    foreach($dt as $k => $v)
     $dt[$k] = htmlspecialchars($dt[$k]);

    $phones  = "����������: (" . $_POST['phone_pref']      . ") " . $_POST['phone']      . "\n";
    $phones .= "��������: ("   . $_POST['dom_phone_pref']  . ") " . $_POST['dom_phone']  . "\n";
    $phones .= "�������: ("    . $_POST['work_phone_pref'] . ") " . $_POST['work_phone'] . "\n";

    // �������!!!!
    $mag = 0; // fav-shop
    // $mag = 1; // pleerbox

    // ������� ������ � ��
    $res = mysql_qw('INSERT INTO '._DB_ZAKAZ.' SET
                      name        = ?,
                      tovary      = ?,
                      cost        = ?,
                      data        = ?,
                      phones      = ?,
                      status      = 0,
                      cl_comment  = ?,
                      mag         = ?,
                      address     = ?,
                      from_site   = "sitimobile.ru"',
                      $dt['name'],
                      $order,
                      $dt['cost_all'],
                      $dt['time'],
                      $phones,
                      $dt['comment'],
                      $mag,
                      $dt['address'])
            or die(mysql_error());

    $zakaz_id = mysql_insert_id();

    // ������ ����� � �� ������
    $cook = $_COOKIE['mobile'];
    if(get_magic_quotes_gpc())
     $cook = stripslashes($cook);
    $mobile_b = unserialize($cook);

    foreach($mobile_b as $k => $v)
    {
     $res = mysql_qw('INSERT INTO '._DB_ZAKAZ_TOV.'
                       SET
                        mobile_id = ?,
                        zakaz_id  = ?,
                        num       = ?',
                        $k, $zakaz_id, $v
                       )
               or die(mysql_error());
    }

    //������ ����������
    $text = file_get_contents("mailtpl/send_order.tpl");
    $text = strtr($text, array(
            "{NAME}"     => $_POST['name'],
            "{ORDER}"    => $order,
            "{SITENAME}" => _CFG_SITENAME,
            ));

    mail($_POST['mail'], iconv('windows-1251', 'utf-8', '��� ����� ������!'), iconv('windows-1251', 'utf-8', $text),
         join("\r\n", array (
                            "From: robot@sitimobile.ru",
                            "Content-type: text/plain; charset=utf-8"))
         );

   // ������ ������
   $text = file_get_contents("mailtpl/send_order_admin.tpl");
   $text = strtr($text, array(
            "{ORDER}"           => $order,
            "{NAME}"            => $_POST['name'],
            "{PHONE-PREF}"      => $_POST['phone_pref'],
            "{PHONE}"           => $_POST['phone'],
            "{DOM_PHONE-PREF}"  => $_POST['dom_phone_pref'],
            "{DOM_PHONE}"       => $_POST['dom_phone'],
            "{WORK_PHONE-PREF}" => $_POST['work_phone_pref'],
            "{WORK_PHONE}"      => $_POST['work_phone'],
            "{MAIL}"            => $_POST['mail'],
            "{TIME}"            => $_POST['time'],
            "{ADDRESS}"         => $_POST['address'],
            "{COMMENT}"         => $_POST['comment'],
            ));

    foreach($adm_mail as $k => $v)
    {
      if(trim($v)!="")
      {
        mail($v, iconv('windows-1251', 'utf-8', '����� �����! (sitimobile.ru)'), iconv('windows-1251', 'utf-8', $text),
             join("\r\n", array (
                                "From: " . $v,
                                "Content-type: text/plain; charset=utf-8"))
            );
      }
    }

    setcookie("mobile", "", 0, "/");
    unset($_SESSION['out']);
    unset($_SESSION['cost_all']);
	unset($_SESSION['delivery']);

    Slow_Redirect("/",2,"�������, ��� ����� ������! ������ �� ������ �������������� �� ������� ��������");
}
elseif(@$_REQUEST['DoSendOrder'] && !CheckData())
 SendOrderForm(2);
elseif(@$_REQUEST['DoSendOrder'] && !isset($_POST['iagree']))
 SendOrderForm(1);
elseif(isset($_SESSION['out']))
 SendOrderForm();

function CheckData()
{
 $dt = $_POST;

 if(trim($dt['name'])       != "" &&
    trim($dt['phone_pref']) != "" &&
    trim($dt['phone'])      != "" )
  return true;
 else
  return false;
}

// ����� ������
// $ui - ���� �����
function SendOrderForm($not_agree=0)
{
?>
<?if($not_agree==1) { ?>
<strong>������!</strong> �� ������ ����������� ���� �����.<br/><br/>
<? } ?>
<?if($not_agree==2) { ?>
<strong>������!</strong> ����������, ��������� ����������� ����.<br/><br/>
<? } ?>
<form action="" method="post">
 <table border=0>
  <tr>
   <td width="180">
    ����� ��������� ������:
   </td>
   <td>
    <strong><?=$_SESSION['cost_all'];?> ���.</strong>
    <input name="cost_all" type="hidden" value="<?=$_SESSION['cost_all'];?>">
   </td>
  </tr>
  <tr>
  <tr height="10"><td colspan="2"></td></tr>
  <tr>
   <td>
    ��������: <span style="color:#ff0000">*</span>
   </td>
   <td>
    <input type="text" name="name" value="<?=@$_POST['name']?>" size="50">
   </td>
  </tr>
  <tr height="10"><td colspan="2"></td></tr>
  <tr>
   <td>
    ����������� �����:
   </td>
   <td>
    <input type="text" name="mail" value="<?=@$_POST['mail']?>" size="50">
   </td>
  </tr>
  <tr height="10"><td colspan="2"></td></tr>
  <tr>
   <td>
    ���������� �������: <span style="color:#ff0000">*</span>
   </td>
   <td>
    (<input type="text" name="phone_pref" value="<?=@$_POST['phone_pref']?>" size="3">) <input type="text" name="phone" value="<?=@$_POST['phone']?>" size="10">
   </td>
  </tr>
  <tr height="10"><td colspan="2"></td></tr>
  <tr>
   <td>
    �������� �������:
   </td>
   <td>
    (<input type="text" name="dom_phone_pref" value="<?=@$_POST['dom_phone_pref']?>" size="3">) <input type="text" name="dom_phone" value="<?=@$_POST['dom_phone']?>" size="10">
   </td>
  </tr>
  <tr height="10"><td colspan="2"></td></tr>
  <tr>
   <td>
    ������� �������:
   </td>
   <td>
    (<input type="text" name="work_phone_pref" value="<?=@$_POST['work_phone_pref']?>" size="3">) <input type="text" name="work_phone" value="<?=@$_POST['work_phone']?>" size="10">
   </td>
  </tr>
  <? if (!empty($_SESSION['delivery'])): ?>
  <tr height="10"><td colspan="2"></td></tr>
  <tr>
   <td>
    ��������:
   </td>
   <td>
	   <b><?=$_SESSION['delivery']['label']?></b>
   </td>
  </tr>
  <? endif; ?>
  <tr height="10"><td colspan="2"></td></tr>
  <tr>
   <td>
    ����� ��������:
   </td>
   <td>
    <textarea name="address" rows="5" cols="50"><?=@$_POST['address']?></textarea>
   </td>
  </tr>
  <tr height="10"><td colspan="2"></td></tr>
  <tr>
   <td>
    �������� ����� ��������:
   </td>
   <td>
    <input type="text" name="time" value="<?=@$_POST['time']?>" size="50">
   </td>
  </tr>
  <tr height="10"><td colspan="2"></td></tr>
  <tr>
   <td>
    ����������:
   </td>
   <td>
    <textarea name="comment" rows="5" cols="50"><?=@$_POST['comment']?></textarea>
   </td>
  </tr>
  <tr height="10"><td colspan="2"></td></tr>
  <tr>
   <td></td>
   <td>
    <input name="iagree" type="checkbox"> � ����������� ���� ����� <br/><br/>
   </td>
  </tr
  <tr height="10"><td colspan="2"></td></tr>
  <tr height="50">
   <td></td>
   <td>
     <input type="submit" name="DoSendOrder" value="��������">
   </td>
  </tr>
 </table>
</form>
<?php
}
?>