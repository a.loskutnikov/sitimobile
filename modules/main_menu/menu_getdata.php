<?php
require_once("mysql_connect.php");
require_once("lib/mysql_qw.php");
require_once("lib/dumper.php");
require_once("admin/ftk_mod_users.php");

function GetMenuData($is_article)
{
  if(!isset($_COOKIE['ftk_gb_user']))
  {
    $res = mysql_qw("SELECT name, id FROM "._DB_STATIC_CONTENT."
                    WHERE parent_id=0 AND is_hidden=0 AND is_article=? ORDER BY pos ASC",
                    $is_article)
          or die(mysql_error());
  }
  elseif(GetUserStatus()=="admin")
  {
    $res = mysql_qw("SELECT name, id FROM "._DB_STATIC_CONTENT."
                    WHERE parent_id=0  AND is_article=? ORDER BY pos ASC",
                    $is_article)
          or die(mysql_error());
  }
  else
  {
    $user_id = GetUserIdByName();
   $res = mysql_qw("SELECT DISTINCT
                       s.name,
                       s.id
                      FROM
                       "._DB_STATIC_CONTENT." as s,  "._DB_ADM_RIGHTS." as adm
                      WHERE
                       (s.is_hidden  = 0 AND
                        s.is_article = ? AND
                        s.parent_id  = 0)
                       OR
                       (s.is_hidden = 1 AND
                        s.parent_id = 0 AND
                        s.is_article = ? AND
                        s.id = adm.razd_id AND
                        adm.type='read_static' AND
                        adm.user_id = ?)
                      ORDER BY s.pos ASC", $is_article, $is_article, $user_id)
          or die(mysql_error());
  }

  for ($menu=array(); $row=mysql_fetch_array($res); $menu[]=$row);

  return $menu;
}
?>