<?php
require_once ("mysql_connect.php");
require_once "lib/dumper.php";
require_once "lib/mysql_qw.php";
require_once "lib/redirect.php";
require_once "admin/ftk_system_lib.php";

$result = mysql_qw('SELECT name, id FROM '._DB_TYPE) or die(mysql_error());
for ($vendor=array(); $row=mysql_fetch_array($result); $vendor[]=$row);

// $m - ������
function GetVendorName($m,$bDesc=0, $bFullInfo=0)
{
    global $vendor;
    $vend = "";
    foreach($vendor as $v)
    {
        if($v['id']==$m['id'])
        {
            if(!$bDesc)
             $vend = $v['name'];
            else
             $vend = $v['vendor_desc'];

            if($bFullInfo)
             $vend = $v;

            break;
        }
    }
    return $vend;
}

function GetPriceRUR($price_usd)
{
    return ceil($price_usd * GetOption('currency_usd'));
}

function GetPriceUSD($price_rub)
{
    return ceil($price_rub / GetOption('currency_usd') * 100) / 100;
}

function GetPath()
{

 $vendors = GetParentVendors();

 $vend2ret['title'] = " ";
 $vend2ret['path']  = " ";
 for($i=1; $i<count($vendors);  $i++)
 {
   $res = mysql_qw('SELECT name, id FROM '._DB_TYPE.' WHERE id=?', $vendors[$i])
           or die(mysql_error());
   $vend = mysql_fetch_array($res);

   if($i==count($vendors)-1)
    $glue = "";
   else
    $glue = " - ";

   $vend2ret['title'] .= $vend['name'] . $glue;
   $vend2ret['path'] .= "<a href='/catalog/".$vend['id']."/' class='path'>" . $vend['name'] . "</a> &gt; ";
 }

 return $vend2ret;
}

function GetParentVendors()
{
 $parent_id = @$_GET['type_id'];
 $vendors[] = $parent_id;

 do{
    $res = mysql_qw('SELECT parent_id FROM '._DB_TYPE.'
                     WHERE id=?', $parent_id)
                     or die("������: " . mysql_error());
    $vendor = mysql_fetch_array($res);
    $parent_id = $vendor['parent_id'];
    $vendors[] = $parent_id;
 } while($parent_id!=0);

 $vendors = array_reverse($vendors);

 return $vendors;
}

function GetModus($m_id, $type_id, $is_primary=0)
{
 if($is_primary==1)
  $primary = "m.is_primary = 1    AND";
 else
  $primary = "";

 $res = mysql_qw('SELECT mm.value, m.name
                  FROM '._DB_MODUS.' AS m, '._DB_MOBILE_MODUS.' AS mm
                  WHERE
                  '.$primary.'
                   m.parent_id  = ?    AND
                   mm.modus_id  = m.id AND
                   mm.mobile_id = ?
                  ORDER BY m.pos ASC
                  ', $type_id, $m_id)
          or die(mysql_error());
 for ($modus=array(); $row=mysql_fetch_array($res); $modus[]=$row);

 return $modus;
}

function ProccessSravn()
{
 if(!is_numeric($_GET['type_id']) || !is_numeric($_GET['mobile_id']))
  die();

 $cook = $_COOKIE;
 $exp = time() + 1*60*60*24; // �������� ����� �����
 $razd= "-";

 if(!isset($_GET['delete'])) // ���� �� ��������� �����
 {
    // ���� � ��� ��� ��� ������� ��� ��������� ��� ������ ����� ���
    if(!isset($cook['type_id']) ||
        (isset($cook['type_id']) && $cook['type_id'] != $_GET['type_id']))
    {
      setcookie("type_id", $_GET['type_id'], $exp);
      setcookie("mob_ids", $razd.$_GET['mobile_id'], $exp);
    }
    else // ��������� ����� � ������������� ����
    {
      $mob_ids = explode($razd,$cook['mob_ids']);

      $is_add = true;
      foreach($mob_ids as $v) // ���������, �� ���� �� ������ ���
       if($v==$_GET['mobile_id'])
        $is_add = false;

      if($is_add)
      {
       if(count($mob_ids)<6)
        $mob_ids[] = $_GET['mobile_id'];
       else
        $mob_ids[5] = $_GET['mobile_id'];

       $mob_ids = implode($razd,$mob_ids);

       setcookie("type_id", $cook['type_id'], $exp);
       setcookie("mob_ids", $mob_ids, $exp);
      }
    }
 }
 else // ���� �� ������� �����
 {
     $mob_ids = explode($razd,$cook['mob_ids']);

     foreach($mob_ids as $v)
      if($v!=$_GET['mobile_id'])
       $new_mob_ids[] = $v;

     $mob_ids = implode($razd,$new_mob_ids);

      setcookie("type_id", $cook['type_id'], $exp);
      setcookie("mob_ids", $mob_ids, $exp);
 }

 Redirect("/?page=sravn&type_id=". $_GET['type_id']);
}

function GetSize($p_id)
{
  // ������� �����
  $res = mysql_qw('SELECT * FROM '._DB_PHOTO.'
                     WHERE photo_id=?', $p_id)
             or die(mysql_error());

  $photo     = mysql_fetch_array($res);
  $photo_big = $photo['photo'];
  $isize     = getimagesize(_CFG_HOST ."/" . $photo_big);

  return $isize;
}

?>