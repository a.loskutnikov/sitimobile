<?php
// -----------------------------------------
// ���������
// -----------------------------------------
require_once("mysql_connect.php");
require_once("ftk_config.php");
require_once("lib/mysql_qw.php");
require_once("lib/dumper.php");
require_once("modules/catalog/catalog_lib.php");

setlocale(LC_ALL,"ru_RU.CP1251");

// -----------------------------------------
// ���������
// -----------------------------------------

Header("Content-type: text/html\n\n");

$date = date("Y-m-d h:i");

echo '<?xml version="1.0" encoding="WINDOWS-1251"?>
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">
<yml_catalog date="'.$date.'">
  <shop>
    <name>Sitimobile.ru</name>
    <company>Sitimobile.ru</company>
    <url>http://www.sitimobile.ru</url>
    <currencies>
      <currency id="RUR" rate="1"/>
    </currencies>
    <categories>';

$res = mysql_qw('SELECT id, name FROM '._DB_TYPE.' ORDER BY id ASC')
         or die(mysql_error());
for ($cats=array(); $row=mysql_fetch_array($res); $cats[]=$row);
foreach ($cats as $v)
 echo '
      <category id="'.$v['id'].'">'.clean_var($v['name']).'</category>';
echo '
    </categories>
    <offers>';

$i = 0;
foreach($cats as $cat)
{
   $res = mysql_qw('SELECT * FROM '._DB_MOBILE.' WHERE parent_id = ? AND is_have=1 ORDER BY id ASC',
                    $cat['id'])
           or die(mysql_error());
   for ($mobiles=array(); $row=mysql_fetch_array($res); $mobiles[]=$row);
   foreach($mobiles as $m)
   {
     $res = mysql_qw('SELECT photo_prv FROM '._DB_PHOTO.'
                      WHERE class="mobile" AND mobile_id = ?
                      ORDER BY pos ASC LIMIT 1', $m['id'])
             or die(mysql_error());

     if(mysql_num_rows($res)==1)
      $photo = mysql_result($res, 0);
     else
      $photo = "";
      
      $m['cost2'] = ceil(($m['currency'] == 840 ? GetPriceRUR($m['cost2']) : $m['cost2']) * 1.03);

     echo '
      <offer id="'.$i.'" type="vendor.model" available="true">
        <url>http://www.sitimobile.ru/catalog/'.$cat['id'].'/'.$m['id'].'/</url>
        <price>'.$m['cost2'].'</price>
        <currencyId>RUR</currencyId>
        <categoryId>'.$cat['id'].'</categoryId>
        <picture>http://www.sitimobile.ru/'.$photo.'</picture>
        <vendor>'.$cat['name'].'</vendor>
        <vendorCode>0</vendorCode>
        <model>'.clean_var($m['name']).'</model>
        <description>'.clean_var($m['short_descr']).'</description>
        <sales_notes>������ ������ �� ������� - '.ceil($m['cost2']*0.1).' ���.</sales_notes>
      </offer>';
     $i++;
   }
}

echo '
    </offers>
  </shop>
</yml_catalog>';


function clean_var($var)
{
 $var = str_replace("&", "&amp;", $var);

 return $var;
}
?>