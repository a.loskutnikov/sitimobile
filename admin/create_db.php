<?php
## ----------------------------------------
##
## ������ �������� ��
##
## ----------------------------------------
require_once "../ftk_config.php";
require_once "../mysql_connect.php";
require_once "../lib/dumper.php";
require_once "../lib/mysql_qw.php";


// ������� ������� � ��������������
// status = admin;
//          moder;
//          zgost;
mysql_qw('
  CREATE TABLE IF NOT EXISTS '._DB_ADMIN.' (
    id        INT AUTO_INCREMENT PRIMARY KEY,
    real_name VARCHAR(60) NOT NULL,
    name      VARCHAR(60) NOT NULL,
    pass      VARCHAR(60) NOT NULL,
    mail      VARCHAR(60) NOT NULL,
    status    VARCHAR(60) NOT NULL
  )
') or die("������ �������� ��: " . mysql_error());

// type = adm_static;
//        adm_catalog;
//        read_static;
mysql_qw('
    CREATE TABLE IF NOT EXISTS '._DB_ADM_RIGHTS.' (
     id      INT AUTO_INCREMENT PRIMARY KEY,
     type    VARCHAR(60) NOT NULL,
     razd_id INT NOT NULL,
     user_id INT NOT NULL
    )
 ')
         or die(mysql_error());

// ������� TYPE
  mysql_qw('
  CREATE TABLE IF NOT EXISTS '._DB_TYPE.' (
    id        INT AUTO_INCREMENT PRIMARY KEY,
    name      TEXT NOT NULL,
    descr     TEXT NOT NULL,
    pos       INT  NOT NULL,
    is_hidden INT  NOT NULL,
    parent_id INT NOT NULL
  )
') or die("������ �������� ��: " . mysql_error());

// ������� MODUS
  mysql_qw('
  CREATE TABLE IF NOT EXISTS '._DB_MODUS.' (
    id          INT AUTO_INCREMENT PRIMARY KEY,
    name        TEXT NOT NULL,
    pos         INT  NOT NULL,
    is_primary  INT  NOT NULL,
    parent_id   INT NOT NULL
  )
') or die("������ �������� ��: " . mysql_error());

// ������� MOBILE_MODUS
  mysql_qw('
  CREATE TABLE IF NOT EXISTS '._DB_MOBILE_MODUS.' (
    id        INT AUTO_INCREMENT PRIMARY KEY,
    value     TEXT NOT NULL,
    mobile_id INT NOT NULL,
    modus_id  INT NOT NULL
  )
') or die("������ �������� ��: " . mysql_error());

// ������� MOBILE
  mysql_qw('
  CREATE TABLE IF NOT EXISTS '._DB_MOBILE.' (
    id        INT AUTO_INCREMENT PRIMARY KEY,
    name      TEXT NOT NULL,
    descr     TEXT NOT NULL,
    parent_id INT  NOT NULL,
    cost      FLOAT NOT NULL,
    pos       INT  NOT NULL,
    flag      INT  NOT NULL
  )
') or die("������ �������� ��: " . mysql_error());

// acc_depence
/*  parent_id - �����, �������� ������ ���������
    acc_id - �����, ����������� �����������
    vendor_id - ��� ����� ;) */
  mysql_qw('
  CREATE TABLE IF NOT EXISTS '._DB_ACC_DEPENCE.' (
    id        INT AUTO_INCREMENT PRIMARY KEY,
    parent_id INT NOT NULL,
    acc_id    INT NOT NULL,
    vendor_id INT NOT NULL,
    pos       INT NOT NULL
  )
') or die("������ �������� ��: " . mysql_error());

// ������� SPEC
  mysql_qw('
  CREATE TABLE IF NOT EXISTS '._DB_SPEC.' (
    spec_id     INT AUTO_INCREMENT PRIMARY KEY,
    spec_name   TEXT NOT NULL,
    spec_desc   TEXT NOT NULL
  )
') or die("������ �������� ��: " . mysql_error());

// ������� PHOTO
  mysql_qw('
  CREATE TABLE IF NOT EXISTS '._DB_PHOTO.' (
    photo_id    INT AUTO_INCREMENT PRIMARY KEY,
    photo       TEXT NOT NULL,
    photo_prv   TEXT NOT NULL,
    mobile_id   INT  NOT NULL,
    descr       TEXT NOT NULL,
    class       TEXT NOT NULL,
    w           INT  NOT NULL,
    h           INT  NOT NULL,
    open        INT  NOT NULL,
    pos         INT  NOT NULL
  )
') or die("������ �������� ��: " . mysql_error());

  mysql_qw('
  CREATE TABLE IF NOT EXISTS '._DB_NEWS.' (
    id       INT AUTO_INCREMENT PRIMARY KEY,
    name     TEXT NOT NULL,
    text     TEXT NOT NULL,
    dt_day   INT NOT NULL,
    dt_month INT NOT NULL,
    dt_year  INT NOT NULL

  )
') or die("������ �������� ��: " . mysql_error());

// ������� STATIC_CONTENT
  mysql_qw('
  CREATE TABLE IF NOT EXISTS '._DB_STATIC_CONTENT.' (
    id            INT  AUTO_INCREMENT PRIMARY KEY,
    name          TEXT NOT NULL,
    text          TEXT NOT NULL,
    is_frontpage  INT NOT NULL,
    is_hidden     INT  NOT NULL,
    is_article    INT  NOT NULL,    
    parent_id     INT  NOT NULL,
    pos           INT  NOT NULL,
    meta_title    TEXT NOT NULL,
    meta_desc     TEXT NOT NULL,
    meta_keywords TEXT NOT NULL
  )
') or die("������ �������� ��: " . mysql_error());

// ������� options
  mysql_qw('
  CREATE TABLE IF NOT EXISTS '._DB_OPTIONS.' (
    id           INT  AUTO_INCREMENT PRIMARY KEY,
    name         TEXT NOT NULL,
    value        TEXT NOT NULL
  )
') or die("������ �������� ��: " . mysql_error());

// ������� podbor
// modus_id TEXT - ������-��� ����� �������� ���� 14_cost - 14 - �� ����, cost - ����
  mysql_qw('
  CREATE TABLE IF NOT EXISTS '._DB_PODBOR.' (
    id           INT  AUTO_INCREMENT PRIMARY KEY,
    type_id      INT  NOT NULL,
    modus_id     TEXT  NOT NULL,
    is_equal     INT  NOT NULL,
    min1         TEXT NOT NULL,
    max1         TEXT NOT NULL,
    min2         TEXT NOT NULL,
    max2         TEXT NOT NULL,
    min3         TEXT NOT NULL,
    max3         TEXT NOT NULL
  )
') or die("������ �������� ��: " . mysql_error());

 // ������
  mysql_qw('
  CREATE TABLE IF NOT EXISTS '._DB_ZAKAZ.' (
    id           INT   AUTO_INCREMENT PRIMARY KEY,
    name         TEXT  NOT NULL,
    tovary       TEXT  NOT NULL,
    cost         FLOAT NOT NULL,
    data         TEXT  NOT NULL,
    phones       TEXT  NOT NULL,
    status       INT   NOT NULL,
    cl_comment   TEXT  NOT NULL,
    adm_comment  TEXT  NOT NULL,
    mag          TEXT  NOT NULL,
    address      TEXT  NOT NULL

  )
') or die("������ �������� ��: " . mysql_error());

 // ������ � �������
  mysql_qw('
  CREATE TABLE IF NOT EXISTS '._DB_ZAKAZ_TOV.' (
    id           INT   AUTO_INCREMENT PRIMARY KEY,
    mobile_id    INT   NOT NULL,
    zakaz_id     INT   NOT NULL,
    num          INT   NOT NULL

  )
') or die("������ �������� ��: " . mysql_error());

 // ������� ������ �� �����!
  mysql_qw('
  CREATE TABLE IF NOT EXISTS '._DB_NKVD.' (
    id           INT   AUTO_INCREMENT PRIMARY KEY,
    text         TEXT  NOT NULL,
    is_tovar     INT   NOT NULL,
    user_id      INT   NOT NULL,
    time         TEXT  NOT NULL

  )
') or die("������ �������� ��: " . mysql_error());


 $res = mysql_qw('SELECT id FROM '._DB_OPTIONS.'')
         or die(mysql_error());
 if(mysql_num_rows($res)==0)
 {
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "cat_dost_cost"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "cat_free_dost_cost"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "cat_disc1_min"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "cat_disc1_max"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "cat_disc1_perc"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "cat_disc2_min"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "cat_disc2_max"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "cat_disc2_perc"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "cat_disc3_min"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "cat_disc3_max"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "cat_disc3_perc"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "cat_mail"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "feedback_text"')
           or die(mysql_error());


   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "vopros"')
           or die(mysql_error());

   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "otv1"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "otv1_num"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "otv2"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "otv2_num"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "otv3"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "otv3_num"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "otv4"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "otv4_num"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "otv5"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "otv5_num"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "otv6"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "otv6_num"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "otv7"')
           or die(mysql_error());
   mysql_qw('INSERT INTO '._DB_OPTIONS.' SET
             name = "otv7_num"')
           or die(mysql_error());
 }

echo "��������� �� �������!";
?>
