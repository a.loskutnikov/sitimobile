<?php
exit();

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once('ftk_config.php');
require_once('mysql_connect.php');
require_once('lib/mysql_qw.php');

$doc = file_get_contents('avito_cats.xml');
$xml = new SimpleXMLElement($doc);

$m = $i = 0;
$categories = array();

foreach ($xml as $category) {
	$i++;
	$m = $i;
	
	$categories[$i] = array(
		'name' => $category->name->__toString(),
		'parent_id' => 0
	);

    if (isset($category->categories)) {
        foreach ($category->categories->children() as $subcategory) {
            $i++;

            $categories[$i] = array(
                'name' => $subcategory->__toString(),
                'parent_id' => $m
            );
        }
    }
}

mysql_qw('TRUNCATE TABLE fa_avito_cats');

$insert_parts = array();
foreach ($categories as $id=>$category) {
	$insert_parts[] = sprintf('(%d, "%s", %d)', $id, mb_convert_encoding($category['name'], 'cp1251', 'utf-8'), $category['parent_id']);
}

$query = 'INSERT INTO fa_avito_cats VALUES '.join(',', $insert_parts);
mysql_qw($query);

echo mysql_affected_rows();
echo mysql_error();