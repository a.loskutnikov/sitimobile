<?php
require_once "bmp.php";

function CreateImageFromFile($file)
 {
    $info = getimagesize($file);
    switch ($info['mime'])
    {
        case "image/gif":
            $img = ImageCreateFromGif($file);
            break;
        case "image/jpeg":
            $img = ImageCreateFromJpeg($file);
            break;
        case "image/png":
            $img = ImageCreateFromPng($file);
            break;
        case "image/bmp":
            $img = ImageCreateFromBMP($file);
            break;
        default:
            $img = false;
            break;
    }

    return $img;
}
?>
