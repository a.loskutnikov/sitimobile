<?php
## ----------------------------------------
##
## ������� ��� ����������� ������ � e-mail
##
## ----------------------------------------

// ������� ��������� ������ ��� preg_replace_callback().
function hrefCallback($p)
{
  // ����������� ����������� � HTML-�������������.
  $name = htmlspecialchars($p[0]);
  // ���� ��� ���������, ��������� ��� � ������ ������.
  $href = !empty($p[1])? $name : "http://$name";
  // ��������� ������.
  return "<a href=\"$href\" target=new>$name</a>";
}

// �������� ������ �� �� HTML-����������� ("������������ ������").
function hrefActivate($text)
{
  return preg_replace_callback(
    '{
      (?:
        (\w+://)          # �������� � ����� �������
        |                 # - ��� -
        www\.             # ������ ���������� �� www
      )
      [\w-]+(\.[\w-]+)*   # ��� �����
      (?: : \d+)?         # ���� (�� ����������)
      [^<>"\'()\[\]\s]*   # URI (�� ��� ������� � ������)
      (?:                 # ��������� ������ ������ ����...
          (?<! [[:punct:]] )  # �� �����������
        | (?<= [-/&+*]     )  # �� ��������� ��������� �� -/&+*
      )
    }xis',
    "hrefCallback",
    $text
  );
}

function mailActivate($text)
{
    $html = preg_replace('/(\\S+)@([a-z0-9.]+)(\\.)([a-z0-9]+)/is',
                     "<a href='mailto:$0'>$0</a>",
                     $text);

    return $html;
}

function tagBIUActivate($text)
{
    $re = '|\[b\] (.*?)  \[/b\]|ixs';
    $to  = '<b>$1</b>';
    $text = preg_replace($re, $to, $text);

    $re = '|\[i\] (.*?)  \[/i\]|ixs';
    $to  = '<i>$1</i>';
    $text = preg_replace($re, $to, $text);

    $re = '|\[u\] (.*?)  \[/u\]|ixs';
    $to  = '<u>$1</u>';
    $text = preg_replace($re, $to, $text);

    return $text;
}
?>